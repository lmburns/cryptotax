You can use this transaction report to inform your likely tax obligations. For US                      From: 2012-01-01T00:00:00Z                 : all    tthudium@gmail.com
2020-05-            Buy             BTC     0.00201928         9166.63                        $18.51          $20.00                    Bought 0.00201928 BTC for $20.00 USD
2020-06-            Buy             BTC     0.02071895         9508.69                        $197.01         $200.00                   Bought 0.02071895 BTC for $200.00 USD
2020-07-            Sell            BTC     0.02069454         9175.85                        $189.89         $186.90                   Sold 0.02069454 BTC for $186.90 USD
2020-10-            Buy             BTC     0.00346265         13865.10                       $48.01          $50.00                    Bought 0.00346265 BTC for $50.00 USD
2020-06-            Buy             ETH     0.81330063         242.24                         $197.01         $200.00                   Bought 0.81330063 ETH for $200.00 USD
2020-07-       Sell          ETH     0.64963243        237.64                         $154.38         $151.39                  Sold 0.64963243 ETH for $151.39 USD
2020-08-       Buy           ETH     0.04364641        424.09                         $18.51          $20.00                   Bought 0.04364641 ETH for $20.00 USD
2020-10-       Buy           ETH     0.12943303        370.93                         $48.01          $50.00                   Bought 0.12943303 ETH for $50.00 USD
2020-11-       Buy           ETH     0.42656914        461.85                         $197.01         $200.00                  Bought 0.42656914 ETH for $200.00 USD
2020-12-       Buy           ETH     0.34395426        572.78                         $197.01         $200.00                  Bought 0.34395426 ETH for $200.00 USD
2020-12-       Buy           ETH     0.33624331        585.92                         $197.01         $200.00                  Bought 0.33624331 ETH for $200.00 USD
2020-12-       Buy           ETH     0.34080294        578.08                         $197.01         $200.00                  Bought 0.34080294 ETH for $200.00 USD
2020-12-            Buy           ETH     0.09734233        739.76                          $72.01          $75.00                   Bought 0.09734233 ETH for $75.00 USD
2021-01-            Sell          ETH     1.54491198        1253.12                         $1,935.96 $1,907.11                      Sold 1.54491198 ETH for $1,907.11 USD
2021-01-            Buy           ETH     0.12817661        1341.98                         $172.01         $175.00                  Bought 0.12817661 ETH for $175.00 USD
2021-01-            Buy           ETH     0.18709466        1053.00                         $197.01         $200.00                  Bought 0.18709466 ETH for $200.00 USD
2021-01-11T17:14:19Z Buy          ETH     0.210346          936.60                          $197.01         $200.00                  Bought 0.210346 ETH for $200.00 USD
2020-06-            Buy           LTC     4.50332761        43.75                           $197.01         $200.00                  Bought 4.50332761 LTC for $200.00 USD
2020-07-            Sell          LTC     4.50332761        43.45                           $195.68         $192.69                  Sold 4.50332761 LTC for $192.69 USD
2020-12-            Buy           XRP     367.716847        0.535800                        $197.01         $200.00                  Bought 367.716847 XRP for $200.00 USD
2020-12-            Buy           XRP     722.025233        0.272900                        $197.01         $200.00                  Bought 722.025233 XRP for $200.00 USD
2020-12-            Sell          XRP     1089.74208        0.224200                        $244.29         $240.65                  Sold 1,089.74208 XRP for $240.65 USD
2020-12-            Coinbase Earn XLM     13.295132         0.150000                        $1.99           $1.99                    Received 13.295132 XLM from Coinbase Earn
2020-12-            Coinbase Earn XLM     13.295132         0.150000                        $1.99           $1.99                    Received 13.295132 XLM from Coinbase Earn
2020-12-       Coinbase Earn XLM     13.2980492        0.150000                        $1.99           $1.99                    Received 13.2980492 XLM from Coinbase Earn
2020-12-       Coinbase Earn XLM     13.3067199        0.150000                        $2.00           $2.00                    Received 13.3067199 XLM from Coinbase Earn
2020-12-       Coinbase Earn XLM     13.3067199        0.150000                        $2.00           $2.00                    Received 13.3067199 XLM from Coinbase Earn
2020-12-       Sell          XLM     66.501753         0.138600                        $9.22           $8.23                    Sold 66.501753 XLM for $8.23 USD
2020-06-       Coinbase Earn EOS     0.7797            2.56                            $2.00           $2.00                    Received 0.7797 EOS from Coinbase Earn
2020-06-       Coinbase Earn EOS     0.7797            2.56                            $2.00           $2.00                    Received 0.7797 EOS from Coinbase Earn
2020-06-       Coinbase Earn EOS     0.7797            2.56                            $2.00           $2.00                    Received 0.7797 EOS from Coinbase Earn
2020-06-       Coinbase Earn EOS     0.7797            2.56                            $2.00           $2.00                    Received 0.7797 EOS from Coinbase Earn
2020-06-       Coinbase Earn EOS     0.7797            2.56                            $2.00           $2.00                    Received 0.7797 EOS from Coinbase Earn
2020-12-       Sell          EOS     3.8985            2.63                            $10.24          $8.75                    Sold 3.8985 EOS for $8.75 USD
2020-11-       Buy           XTZ     45.775768         2.12                            $97.01          $100.00                  Bought 45.775768 XTZ for $100.00 USD
2020-11-       Buy           XTZ     45.005299         2.16                           $97.01          $100.00                  Bought 45.005299 XTZ for $100.00 USD
2020-12-       Buy           XTZ     106.248126        2.32                           $246.33         $250.00                  Bought 106.248126 XTZ for $250.00 USD
2020-12-       Buy           XTZ     94.83327          2.08                           $197.01         $200.00                  Bought 94.83327 XTZ for $200.00 USD
2020-12-       Rewards       XTZ     0.015987          2.38                           $0.04           $0.04                    Received 0.015987 XTZ from Coinbase Rewards
2020-12-       Rewards       XTZ     0.016169          2.30                           $0.04           $0.04                    Received 0.016169 XTZ from Coinbase Rewards
2020-12-       Rewards       XTZ     0.032072          1.97                           $0.06           $0.06                    Received 0.032072 XTZ from Coinbase Rewards
2020-12-       Buy           XTZ     106.723568        1.85                           $197.01         $200.00                  Bought 106.723568 XTZ for $200.00 USD
2020-12-       Rewards       XTZ     0.032909          2.02                           $0.07           $0.07                    Received 0.032909 XTZ from Coinbase Rewards
2020-12-       Rewards       XTZ     0.031724          2.18                           $0.07           $0.07                    Received 0.031724 XTZ from Coinbase Rewards
2020-12-       Rewards       XTZ     0.032696          2.00                           $0.07           $0.07                    Received 0.032696 XTZ from Coinbase Rewards
2021-01-       Sell          XTZ     398.747588        2.07                           $824.06         $811.78                  Sold 398.747588 XTZ for $811.78 USD
2021-01-       Rewards       XTZ     0.030704          2.13                           $0.07           $0.07                    Received 0.030704 XTZ from Coinbase Rewards
2021-01-       Rewards       XTZ     0.032512          2.61                           $0.08           $0.08                    Received 0.032512 XTZ from Coinbase Rewards
2021-01-       Rewards       XTZ     0.03224           2.64                           $0.09           $0.09                    Received 0.03224 XTZ from Coinbase Rewards
2021-01-       Rewards       XTZ     0.031486          2.37                           $0.07           $0.07                    Received 0.031486 XTZ from Coinbase Rewards
2020-12-       Coinbase Earn COMP 0.01900659           157.42                         $2.99           $2.99                    Received 0.01900659 COMP from Coinbase Earn
2020-12-       Coinbase Earn COMP 0.01900659           157.42                         $2.99           $2.99                    Received 0.01900659 COMP from Coinbase Earn
2020-12-       Coinbase Earn COMP 0.01900659           157.42                         $2.99           $2.99                    Received 0.01900659 COMP from Coinbase Earn
2020-12-       Sell          COMP 0.05701977           135.04                         $7.70           $6.71                    Sold 0.05701977 COMP for $6.71 USD
2020-12-       Coinbase Earn BAND    0.15428051        6.47                           $1.00           $1.00                    Received 0.15428051 BAND from Coinbase Earn
2020-12-       Coinbase Earn BAND    0.15428051        6.47                           $1.00           $1.00                    Received 0.15428051 BAND from Coinbase Earn
2020-12-       Coinbase Earn BAND    0.15430908        6.47                           $1.00           $1.00                    Received 0.15430908 BAND from Coinbase Earn
2020-12-       Sell          BAND    0.4628701         5.68                          $2.63            $1.64                    Sold 0.4628701 BAND for $1.64 USD
