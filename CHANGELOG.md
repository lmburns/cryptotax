### fixed (HEAD -> master)
>Tue, 2 Mar 2021 15:35:06 -0600

>Author: Lucas Burns (burnsac@me.com)

>Commiter: Lucas Burns (burnsac@me.com)




### sql table working new (origin/master)
>Fri, 26 Feb 2021 12:57:32 -0600

>Author: Lucas Burns (burnsac@me.com)

>Commiter: Lucas Burns (burnsac@me.com)




### check cointracking.csv
>Fri, 26 Feb 2021 10:22:40 -0600

>Author: Lucas Burns (burnsac@me.com)

>Commiter: Lucas Burns (burnsac@me.com)




### updated some python, think theres errors in the original one
>Fri, 26 Feb 2021 10:13:47 -0600

>Author: Lucas Burns (burnsac@me.com)

>Commiter: Lucas Burns (burnsac@me.com)




### fixed cb-gem.md table
>Thu, 25 Feb 2021 13:47:10 -0600

>Author: Lucas Burns (burnsac@me.com)

>Commiter: Lucas Burns (burnsac@me.com)




### compute currency exchange, your python script addded
>Thu, 25 Feb 2021 13:42:18 -0600

>Author: Lucas Burns (burnsac@me.com)

>Commiter: Lucas Burns (burnsac@me.com)




### Committing for you to see changes before I go to work Problem: Still need `trans_buy_curr` and `trans_sell_curr` to be figured out. Also, fill out the table that I put into `fifo_examples` if you would, please.
>Wed, 24 Feb 2021 12:02:39 -0600

>Author: Lucas Burns (burnsac@me.com)

>Commiter: Lucas Burns (burnsac@me.com)




### Added python folder and fifo document understanding trades Problem: Gemini csv needs worked on
>Wed, 24 Feb 2021 10:29:10 -0600

>Author: Lucas Burns (burnsac@me.com)

>Commiter: Lucas Burns (burnsac@me.com)




### Subject: Added buy currency and sell currency to sql table Problem: Started using gemini csv Authored-by: Lucas Burns <burnsac@me.com>
>Tue, 23 Feb 2021 17:10:54 -0600

>Author: Lucas Burns (burnsac@me.com)

>Commiter: Lucas Burns (burnsac@me.com)




### Fixed a small bug
>Mon, 22 Feb 2021 16:04:10 -0600

>Author: tthudium (tthudium@gmail.com)

>Commiter: GitHub (noreply@github.com)




### Updated PHP
>Mon, 22 Feb 2021 15:57:35 -0600

>Author: tthudium (tthudium@gmail.com)

>Commiter: GitHub (noreply@github.com)

Added login including new database setup


### script to autogenerate data if something happens (requires fake-data-cli)
>Mon, 22 Feb 2021 10:51:28 -0600

>Author: Lucas Burns (burnsac@me.com)

>Commiter: Lucas Burns (burnsac@me.com)




### data inserted with scripts
>Mon, 22 Feb 2021 10:41:04 -0600

>Author: Lucas Burns (burnsac@me.com)

>Commiter: Lucas Burns (burnsac@me.com)




### table to work on php
>Mon, 22 Feb 2021 09:40:35 -0600

>Author: Lucas Burns (burnsac@me.com)

>Commiter: Lucas Burns (burnsac@me.com)




### table fixed, add filename drop foreign keys primary key issues
>Mon, 22 Feb 2021 09:04:42 -0600

>Author: Lucas Burns (burnsac@me.com)

>Commiter: Lucas Burns (burnsac@me.com)




### table final
>Mon, 22 Feb 2021 08:50:04 -0600

>Author: Lucas Burns (burnsac@me.com)

>Commiter: Lucas Burns (burnsac@me.com)




### table and changelog
>Mon, 22 Feb 2021 08:28:49 -0600

>Author: Lucas Burns (burnsac@me.com)

>Commiter: Lucas Burns (burnsac@me.com)




### table && changelog
>Mon, 22 Feb 2021 08:21:19 -0600

>Author: Lucas Burns (burnsac@me.com)

>Commiter: Lucas Burns (burnsac@me.com)




### cleaned stashed changes, tables.sql.bak is backup of other
>Sun, 21 Feb 2021 22:33:06 -0600

>Author: Lucas Burns (burnsac@me.com)

>Commiter: Lucas Burns (burnsac@me.com)




### damn cant figure out stash (origin/test, test)
>Sun, 21 Feb 2021 22:12:36 -0600

>Author: Lucas Burns (burnsac@me.com)

>Commiter: Lucas Burns (burnsac@me.com)




### tables.sql fixes
>Sun, 21 Feb 2021 21:36:25 -0600

>Author: tthudium (tthudium@gmail.com)

>Commiter: GitHub (noreply@github.com)

- removed a primary key in crypto.users and crypto.documents
- added filename column to crypto.users


### test database insert
>Sun, 21 Feb 2021 19:57:26 -0600

>Author: Lucas Burns (burnsac@me.com)

>Commiter: Lucas Burns (burnsac@me.com)




### really should work now
>Sun, 21 Feb 2021 18:13:24 -0600

>Author: Lucas Burns (burnsac@me.com)

>Commiter: Lucas Burns (burnsac@me.com)




### just up to date sql table for you to see
>Sun, 21 Feb 2021 18:02:11 -0600

>Author: Lucas Burns (burnsac@me.com)

>Commiter: Lucas Burns (burnsac@me.com)




### fixed table
>Sun, 21 Feb 2021 16:55:51 -0600

>Author: Lucas Burns (burnsac@me.com)

>Commiter: Lucas Burns (burnsac@me.com)




### mysql table working setup
>Sun, 21 Feb 2021 16:37:54 -0600

>Author: Lucas Burns (burnsac@me.com)

>Commiter: Lucas Burns (burnsac@me.com)




### organized table where it should work, inserting data now
>Sat, 20 Feb 2021 22:06:08 -0600

>Author: Lucas Burns (burnsac@me.com)

>Commiter: Lucas Burns (burnsac@me.com)




### fake data
>Fri, 19 Feb 2021 16:55:40 -0600

>Author: Lucas Burns (burnsac@me.com)

>Commiter: Lucas Burns (burnsac@me.com)




### cleaned & going to bed
>Fri, 19 Feb 2021 01:06:51 -0600

>Author: Lucas Burns (burnsac@me.com)

>Commiter: Lucas Burns (burnsac@me.com)




### created tables, sql insert
>Fri, 19 Feb 2021 00:42:13 -0600

>Author: Lucas Burns (burnsac@me.com)

>Commiter: Lucas Burns (burnsac@me.com)




### table for sql, deleted dollar signs to use as int
>Thu, 18 Feb 2021 21:18:20 -0600

>Author: Lucas Burns (burnsac@me.com)

>Commiter: Lucas Burns (burnsac@me.com)




### sql insert
>Thu, 18 Feb 2021 20:36:30 -0600

>Author: Lucas Burns (burnsac@me.com)

>Commiter: Lucas Burns (burnsac@me.com)




### fixed rewards income
>Thu, 18 Feb 2021 10:40:40 -0600

>Author: Lucas Burns (burnsac@me.com)

>Commiter: Lucas Burns (burnsac@me.com)




### gemini-csv, not much just keeping as current as possible
>Wed, 17 Feb 2021 12:05:13 -0600

>Author: Lucas Burns (burnsac@me.com)

>Commiter: Lucas Burns (burnsac@me.com)




### gemini format
>Tue, 16 Feb 2021 20:00:32 -0600

>Author: Lucas Burns (burnsac@me.com)

>Commiter: Lucas Burns (burnsac@me.com)




### added coinbase to begin, csv support
>Tue, 16 Feb 2021 18:53:24 -0600

>Author: Lucas Burns (burnsac@me.com)

>Commiter: Lucas Burns (burnsac@me.com)




### pdf change directory
>Tue, 16 Feb 2021 17:26:37 -0600

>Author: Lucas Burns (burnsac@me.com)

>Commiter: Lucas Burns (burnsac@me.com)

Signed-off-by: Lucas Burns <burnsac@me.com>



### fixed blank maybe
>Tue, 16 Feb 2021 17:00:33 -0600

>Author: Lucas Burns (burnsac@me.com)

>Commiter: Lucas Burns (burnsac@me.com)




### datetime on line, delete send
>Tue, 16 Feb 2021 16:49:35 -0600

>Author: Lucas Burns (burnsac@me.com)

>Commiter: Lucas Burns (burnsac@me.com)




### cleaned up, added new formats
>Tue, 16 Feb 2021 16:18:40 -0600

>Author: Lucas Burns (burnsac@me.com)

>Commiter: Lucas Burns (burnsac@me.com)




### added replace
>Tue, 16 Feb 2021 13:52:51 -0600

>Author: Lucas Burns (burnsac@me.com)

>Commiter: Lucas Burns (burnsac@me.com)




### awk if else working
>Mon, 15 Feb 2021 12:03:14 -0600

>Author: Lucas Burns (burnsac@me.com)

>Commiter: Lucas Burns (burnsac@me.com)




### buys sells sents
>Mon, 15 Feb 2021 09:52:28 -0600

>Author: Lucas Burns (burnsac@me.com)

>Commiter: Lucas Burns (burnsac@me.com)




### got overall format to scrape
>Mon, 15 Feb 2021 00:20:06 -0600

>Author: Lucas Burns (burnsac@me.com)

>Commiter: Lucas Burns (burnsac@me.com)




### --
>Sat, 13 Feb 2021 20:56:34 -0600

>Author: Lucas Burns (burnsac@me.com)

>Commiter: Lucas Burns (burnsac@me.com)




### Instructions for starting Apache2
>Fri, 12 Feb 2021 20:02:15 -0600

>Author: tthudium (tthudium@gmail.com)

>Commiter: GitHub (noreply@github.com)




### formats to grep
>Fri, 12 Feb 2021 18:46:12 -0600

>Author: Lucas Burns (burnsac@me.com)

>Commiter: Lucas Burns (burnsac@me.com)




### Add files via upload
>Fri, 12 Feb 2021 16:56:34 -0600

>Author: tthudium (tthudium@gmail.com)

>Commiter: GitHub (noreply@github.com)

Basic login/register and upload feature


### $1 input pdfchk, no hardcode
>Fri, 12 Feb 2021 16:48:48 -0600

>Author: Lucas Burns (burnsac@me.com)

>Commiter: Lucas Burns (burnsac@me.com)




### script for owner, email, timestamps
>Fri, 12 Feb 2021 15:24:42 -0600

>Author: Lucas Burns (burnsac@me.com)

>Commiter: Lucas Burns (burnsac@me.com)




### better script with ripgrep
>Fri, 12 Feb 2021 11:36:37 -0600

>Author: Lucas Burns (burnsac@me.com)

>Commiter: Lucas Burns (burnsac@me.com)




### bash script to check for filetype
>Fri, 12 Feb 2021 11:12:33 -0600

>Author: Lucas Burns (burnsac@me.com)

>Commiter: Lucas Burns (burnsac@me.com)




### cleaned up cache
>Fri, 12 Feb 2021 09:27:17 -0600

>Author: Lucas Burns (burnsac@me.com)

>Commiter: Lucas Burns (burnsac@me.com)




### pdfs, sorted tutorial mod TODO
>Fri, 12 Feb 2021 07:33:07 -0600

>Author: Lucas Burns (burnsac@me.com)

>Commiter: Lucas Burns (burnsac@me.com)




### added backup file instead of todo
>Thu, 11 Feb 2021 22:55:08 -0600

>Author: Lucas Burns (burnsac@me.com)

>Commiter: Lucas Burns (burnsac@me.com)




### added git tutorial, sql commands
>Thu, 11 Feb 2021 19:44:25 -0600

>Author: Lucas Burns (burnsac@me.com)

>Commiter: Lucas Burns (burnsac@me.com)




### Update README.md
>Thu, 11 Feb 2021 17:46:12 -0600

>Author: tthudium (tthudium@gmail.com)

>Commiter: GitHub (noreply@github.com)

Tested Github by editing readme


### add git ignore
>Thu, 11 Feb 2021 16:02:34 -0600

>Author: Lucas Burns (burnsac@me.com)

>Commiter: Lucas Burns (burnsac@me.com)




### inital commit
>Thu, 11 Feb 2021 16:02:01 -0600

>Author: Lucas Burns (burnsac@me.com)

>Commiter: Lucas Burns (burnsac@me.com)




